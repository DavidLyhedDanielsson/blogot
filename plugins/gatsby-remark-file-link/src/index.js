var visit = require("unist-util-visit")

module.exports = ({ markdownAST, markdownNode }, pluginOptions) => {
  const demoCommit = markdownNode.frontmatter.demoCommit
  if (!demoCommit) {
    return markdownAST
  }
  const linkRegex = /([a-zA-Z_]+\.gd)#([0-9]+)?$/
  visit(markdownAST, "linkReference", node => {
    const matches = node.label.match(linkRegex)
    if (matches && matches.length >= 2) {
      const file = matches[1]
      const lineNumber = matches[2]

      node.type = "link"
      node.url = `https://gitlab.com/DavidLyhedDanielsson/godot/-/blob/${demoCommit}/scripts/${file}${
        lineNumber ? `#L${lineNumber}` : ""
      }`
      node.children = [
        {
          type: "text",
          value: file,
        },
      ]
    }
  })
  return markdownAST
}
