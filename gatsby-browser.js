// custom typefaces
import "typeface-montserrat"
import "typeface-merriweather"

import "prismjs/themes/prism.css"

import "prismjs/themes/prism-okaidia.css"
//import "prismjs/themes/prism-solarizedlight.css"
import "./src/styles/global.css"
