---
title: Gitting started
date: "2020-03-31"
description: ""
demo: ""
demoText: ""
---

Originally, this post was going to tell you why you should use git and give
you an introduction to it. However, I realized that a git intro is too big
for a single post, and not something I'd like to spend too time on since tons
of resources already exist.

Instead, I will simply say that you will definitely run into git if you work
with software, and actually learning how to use it will benefit you in the
future. Make sure to tell your boss how much knowledge you have, how much
everyone asks you for help, and how you solved some huge git-related issue.
Also, [here's a link](https://try.github.io/) to a page with material to
learn git. I'd suggest starting with
[Learn Git branching](https://learngitbranching.js.org/) if you just want to
get started.

In the beginning it might seem a bit scary to trust git to not lose your
code, but git is actually pretty good at stopping you from
overwriting/removing files. One of the few ways of losing your work is by
using `git reset --hard`.

> Here's a huge secret: You don't have to learn git commands; Google will find
any command you need. Though, following a list of commands without knowing
what you're doing can be a recipe for disaster.

## Style

You'll notice the peculiar format I write my commit messages. I follow
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/), but
you don't have to. In fact, you don't have to follow any standards! But
following some commonly used ones will make it easier for you and other
people to navigate your code and git history. Adopting a standard early on also
simply helps you in the future - because you will know how you were working.

However, any argument about style is inherently meaningless if the argument
turns into "Well, we should put this or that here or there because that's the
way I like it!", which has been the case for most style arguments I've taken
part of. Sometimes there are reasons to do it one way, and if you have no
reasons to oppose that way I would suggest you simply learn to be open-minded.

Common style-related arguments include:
- [Where to put curly braces?](https://softwareengineering.stackexchange.com/questions/2715/should-curly-braces-appear-on-their-own-line) -
Lots of answers here. Some more valid than others. Personally I disagree with
[this reply](https://softwareengineering.stackexchange.com/a/2970) and agree
with
[this reply](https://softwareengineering.stackexchange.com/a/2786).
- [Spaces or tabs?](https://softwareengineering.stackexchange.com/questions/57/tabs-versus-spaces-what-is-the-proper-indentation-character-for-everything-in-e) -
I prefer spaces because then I can reliably align things, and consider mixing
tabs and spaces a crime.
- [Prefix or postfix operators?](https://stackoverflow.com/questions/7031326/what-is-the-difference-between-prefix-and-postfix-operators) -
Turns out to not always be a style question, but a lot of times it is.
- Two or four spaces for indentation? Pro tip: using fewer spaces lets you
  write bigger functions/methods since the code will still fit on one screen.
  Yeah, no, please don't.
- [How many columns should you aim for?](https://softwareengineering.stackexchange.com/questions/604/is-the-80-character-limit-still-relevant-in-times-of-widescreen-monitors)

The list goes on...

## Some examples of what is possible with git

This post is getting long, so here's a quick list you can use as a starting
point for things you might want to do:

- Add (stage) only parts of files to a commit: `git add -p`
- Store your uncomitted changes to tracked files and remove the changes: `git
  stash`
- Restore changes from `git stash`: `git stash apply` and then `git stash
  drop`.
- Change the last created commit (perhaps you forgot to add a change to some
  file): `git commit --amend`
- See contents of a single commit: `git show <commit id>`
- (With no uncommited changes to your code) Make your code exactly like the
  given commit id: `git checkout <commit id>`
- (Even with changes that **will be discarded**) Make your code look exactly
  like the given commit id: `git reset <commit id> --hard`
- Undoing all uncommited changes to a file: `git checkout -- <file path>` or
  with newer versions `git restore <file path>`
- Undoing parts of files: `git checkout -p`
- Unstaging a change you have added with `git add`, without modifying the
  source: `git reset -- <file path>`
- Getting the diff between two commits (note: exactly *two* dots): `git diff
  <ID>^..<ID>`

You can try it out by cloning the game's repo and jumping around with `git
checkout`, and seeing all changes between commits using the `git diff`
command above.

That's it for this short post!
