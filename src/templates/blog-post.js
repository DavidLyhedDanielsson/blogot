import React from "react"
import { Link, graphql } from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm, scale } from "../utils/typography"

/**
 * @param {string} text 
 */
const getDemoPath = (text) => {
  if(text.indexOf('/') !== -1) {
    return `../demos/${text.substring(text.lastIndexOf('/') + 1)}`
  } else {
    return `../demos/${text}`
  }
};

const commitLink = text => {
  if(text) {
    const url = `https://gitlab.com/DavidLyhedDanielsson/godot/-/blob/${text}`
    return <p>
      The commit id for this page's demo is <a href={url}>{text}</a>.
    </p>
  } else {
    return ''
  }
}

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
        />
        <article>
          <header>
            <h1
              style={{
                marginTop: rhythm(1),
                marginBottom: 0,
              }}
            >
              {post.frontmatter.title}
            </h1>
            <p
              style={{
                ...scale(-1 / 5),
                display: `block`,
                marginBottom: rhythm(1),
              }}
            >
              {post.frontmatter.date}
            </p>
            { post.frontmatter.demoCommit && <iframe class="demo" title="Godot demo" src={getDemoPath(post.frontmatter.demoCommit)}></iframe> }
            { post.frontmatter.demoCommit && post.frontmatter.demoText && <p class="demo-text">{post.frontmatter.demoText}</p> }
          </header>
          <section dangerouslySetInnerHTML={{ __html: post.html }} />
          <hr
            style={{
              marginBottom: rhythm(1),
            }}
          />
          { commitLink(post.frontmatter.demoCommit) }
          <p>
            All code will always be available at <a href="https://gitlab.com/DavidLyhedDanielsson/godot">my personal gitlab.</a>
          </p>
          <footer>
            <Bio />
          </footer>
        </article>

        <nav>
          <ul
            style={{
              display: `flex`,
              flexWrap: `wrap`,
              justifyContent: `space-between`,
              listStyle: `none`,
              padding: 0,
            }}
          >
            <li>
              {previous && (
                <Link to={previous.fields.slug} rel="prev">
                  ← {previous.frontmatter.title}
                </Link>
              )}
            </li>
            <li>
              {next && (
                <Link to={next.fields.slug} rel="next">
                  {next.frontmatter.title} →
                </Link>
              )}
            </li>
          </ul>
        </nav>
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        demoCommit
        demoText
      }
    }
  }
`
