---
title: Tile picking 
date: "2020-02-20"
description: "A very soft introduction to picking and some 3D maths.
Preparation for the next post"
demoCommit: "/home/david/dev/godot/blogot/tile-picking"
demoText: "Move the camera with WASD, space, and control. Right click to
rotate the camera, left click to cast a ray. Make sure no plugin (like vimium)
swallows any keypress. Read on for an explanation!"
---

Picking, or should I say ray casting - not to be confused with ray tracing -
is very useful in a lot of situations; both when making both 2D and 3D games.
It's not only used to let the user click on things, but also to check if an
enemy can see the player , if the player is looking at some object, if the
player should be hit by a bullet or grenade, and a lot of other things.

In the game Overgrowth by Wolfire Games, a ray is cast from enemies' eyes to
a random point on the player character. If a certain number of rays hit the
player within a time frame, the enemy has spotted the player.

![Ray casting in the game Overgrowth by Wolfire Games: https://youtu.be/Egy40TYFut8](overgrowth.mp4)

In theory, and also in practice, ray casting is very simple: for each object
in the game world, go through each and every single triangle, and check if
the triangle intersects it. Usually this method is too slow though, and is
accelerated by quadtrees, octrees, simplified meshes used solely for
collision testing, hierarchial meshes (box -> simplified mesh -> "real"
mesh), SIMD instructions, GPU acceleration, or any other of a range of
optimization techniques you can find on google or in literature.

## Moving away from triangles

Since this game essentially takes place on a 2D plane there is no need for
fancy acceleration structures like quadtrees or hierarchial meshes, or even
triangles. Calculating the intersection point between a ray and a 2D plane
ends up being only two lines of code.

Imagine you're standing on a infinitely big white and black checkered floor.
This floor stretches out in the x and z direction, but its height - its y
position - is 0. Your eyes are at `ray_position`. Now,
point at one of the tiles. The direction you're pointing is `ray_direction`.
What we need to figure out is which tile you are pointing at.

Now imagine - but don't put too much thought into it - your eyes fly out of
their sockets in the direction you're pointing . Eventually they would hit
the floor exactly where you're pointing. How do we know when your eyes hit
the floor? Well, you know where your eyes started (`ray_position`), which
direction they were travelling (`ray_direction`), and where the floor is
(anywhere on the x or z axis, 0 on the y axis).

In pseudo-mathematical terms:
```
ray_position + ray_direction * distance = somewhere_on_the_floor
```

The floor is, as defined earlier, infinitely big in the x and z direction. We
know that eventually our eyes will hit the floor somewhere as long as they
don't move parallel to the floor, so we can actually ignore the x and z
components of the previous equation to turn it into:
```
ray_position.y + ray_direction.y * distance = somewhere_on_the_floor.y
```

Since we know `ray_position.y`, `ray_direction.y`, and
`somewhere_on_the_floor.y`, the equation turns into a
intro-to-equations-difficult problem. For instance, if the ray starts at (1,
2, 3), and is shot in the direction (-2, -0.5, 4) the equation becomes:
```
2 + (-0.5) * distance = 0
```

With all this information, we can solve for `distance`. In this case distance
turns out to be 4. Now we know that our eyes have to move 4 units in the
direction we're pointing until it hits the floor, and we can plug it back
into the original equation:
```
ray_position + ray_direction * distance = somewhere_on_the_floor
 (1, 2, 3)   + (-2, -0.5, 4) *    4     = somewhere_on_the_floor
```

And we can calculate exactly where `somewhere_on_the_floor` is. Now we know
exactly where we're pointing.

## Play around in the demo

In the demo you can shoot rays by clicking with the left mouse button, and
then moving the camera with WASD, space, control, and the mouse while holding
down the right mouse button.

After shooting a ray, move the camera around and move your mouse cursor over
the line to see how `ray_position` aka `from`, `ray_direction` aka `dir`, and
`distance` aka `dist` interact. When `dist` reaches its maximum value the
floor has been hit.

Something worth noting is that the origin of the tiles is in the middle. This
means that tiles are placed on (0, 0, 0), (1, 0, 0), (4,0, 7) etc, and that
they extend 0.5 units in both the x and z direction, making the extents of
the tile at (0, 0, 0): (-0.5, 0, -0.5), (0.5, 0, 0.5), and *not* (0, 0, 0),
(1, 0, 1)
