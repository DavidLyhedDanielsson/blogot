import glob
import re
import subprocess
import os
import shutil
from typing import Tuple, List

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
GODOT_PROJECT_PATH = "/home/david/dev/godot/repos/conveyor"
PAGES_PATH = "content/blog"
OUTPUT_PATH = "static/demos"


def find_commits() -> Tuple[List[str], List[str]]:
    commit_id_regex = re.compile(r'demoCommit: "([0-9a-f]{7,})"')
    repo_path_regex = re.compile(r'demoCommit: "([0-9a-zA-Z-_\/]+)"')

    commit_ids = []
    repo_paths = []

    for file_path in glob.glob(PAGES_PATH + "/**/index.md"):
        with open(file_path, "r") as file:
            for line in file:
                commit_id_match = commit_id_regex.match(line)
                if commit_id_match:
                    commit_ids.append(commit_id_match.group(1))
                    continue
                repo_path_match = repo_path_regex.match(line)
                if repo_path_match:
                    repo_paths.append(repo_path_match.group(1))

    return commit_ids, repo_paths


if __name__ == "__main__":
    shutil.rmtree(OUTPUT_PATH)
    os.makedirs(OUTPUT_PATH)
    commit_ids, repo_paths = find_commits()

    print(f"Found {len(commit_ids)} commits and {len(repo_paths)} repos to export...")

    first_iteration = True
    for commit_id in commit_ids:
        commit_target_directory = f"{CURRENT_DIR}/{OUTPUT_PATH}/{commit_id}"
        print(f"Checking out and exporting {commit_id} to {commit_target_directory}...")
        os.mkdir(commit_target_directory)
        command = (f"git checkout {commit_id} && "
                   f"/home/david/.local/share/godot.AppImage "
                   f"--export html5 "
                   f"{commit_target_directory}/index.html && "
                   f"git reset HEAD --hard")
        process = subprocess.Popen(command,
                                   cwd=GODOT_PROJECT_PATH,
                                   stdout=subprocess.PIPE,
                                   shell=True)
        process.wait()

        os.rename(f"{commit_target_directory}/index.wasm",
                  f"{CURRENT_DIR}/{OUTPUT_PATH}/engine.wasm")

    for repo_path in repo_paths:
        repo_name = repo_path.split('/')[-1]
        repo_target_directory = f"{CURRENT_DIR}/{OUTPUT_PATH}/{repo_name}"
        print(f"Exporting repo at {repo_path} as {repo_name} to {repo_target_directory}...")
        os.mkdir(repo_target_directory)
        command = (f"/home/david/.local/share/godot.AppImage "
                   f"--export html5 "
                   f"{repo_target_directory}/index.html")
        process = subprocess.Popen(command,
                                   cwd=repo_path,
                                   stdout=subprocess.PIPE,
                                   shell=True)
        process.wait()

        os.rename(f"{repo_target_directory}/index.wasm",
                  f"{CURRENT_DIR}/{OUTPUT_PATH}/engine.wasm")

    for filePath in glob.glob(OUTPUT_PATH + "/**/index.js"):
        print(f"Replacing engine path in {filePath}")
        with open(filePath, "r") as file:
            fileData = file.read()

        fileData = fileData.replace(r"engineLoadPromise = loadPromise(basePath + '.' + (wasmFilenameExtensionOverride || 'wasm'))",
                                    r"engineLoadPromise = loadPromise('../engine.wasm')")

        with open(filePath, "w") as file:
            file.write(fileData)
