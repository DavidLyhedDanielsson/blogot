---
title: Initial commit with .gitignore
date: "2020-02-22"
description: "Code structure and thinking ahead, creating a very simple
camera, ditching Godot's picking, and the first gameplay element: placing
conveyor belts"
demoCommit: "807aaad"
demoText: "Use WASD, control (beware of ctrl + w!), and space to move the
camera. Click on a conveyor belt, then click on a space next to it to place a
conveyor belt there."
---

The first thing to do when starting a new project is of course to initialize
a git repo in the project directory. Godot plays nice with Git, because a lot
of the files Godot uses are plaintext, which means you can look at the
difference between two files in a normal text editor.

> You don't have to host your github repo anywhere online (github, gitlab,
> bitbucket, etc) to reap the benefits of git, but you will be thankful you did
> when you need the backup.

Each post on this blog will be accompanied by one or more git commits, and
the post will discuss what has been done, why it was done, and how it was
done.

## If you're anything like me...

The first git commit you make in a new project is going to be too big for
what a "good" sized commit should be. In this case, the initial commit
consists of what came out of a handful of hours of experimentation and
familiarization with Godot, rather than a particular and narrow, piece of
work.

But of course, the following commits will always be small, atomic, and
well-defined since you have a very thought-out plan behind it. Except when
you don't, and that's fine too.

## Single responsibility principle

The principle that a class, a file, a unit test, a commit, or really anything
at all when it comes to programming, should only serve a specific purpose
helps the developer (usually yourself, since you're writing the code) over
the course of the project.

I think a lot of people end up writing worldManager.cpp/py/gd/js, which then
ends up a 2000-line behemoth that contains the entirety of the game world,
updates everything, draws everything, and generally manages the world -
making the name a very fitting one. This sort of does not violate the SR
principle since it technically only does one thing: "manages the world". I
think it is a good idea to refrain from using the word "manager" in file or
class names, just to box your mindset in. If you need a class name to contain
"manager", perhaps you've overscoped your class.

## Introducing...

worldManager.gd which does not manage the world, but I could not think of a
better name at the time.

What this "world manager" actually does is that it disconnects the graphical
version of the game world with the logical, in-memory, version. This means
that the world manager contains a 2D array of enums, and this enum array is
later on interpreted as the game world. I can contain variants like `X_Pos`
for a conveyor belt moving things in the positive x direction, `X_Neg`, or
`X_Pos_Z_Pos` which is a corner from `X_Pos` towards `Z_Pos`. For junctions,
a `Cross` variant is used.

What I've ended up with is a file that hides the internal logic of placing
conveyor belts. I call the `add_conveyor` function, and then I can fetch the
new 2D array of the world, and update the graphics so they correspond with
this logical version. With this setup, I can easily change the internal logic
of this file, and even rewrite it in another language if I ever need to do
that. An enum is essentially an int, and it is usually extremely simple to
transfer numbers even between languages and environments.

## Code

The code is mostly just logic broken up into small chunks. If you have
trouble understanding how it works, go through the code line-by-line and
imagine what would happen if the line was removed. Though, some maths may
require some explanation...

## Right-handed coordinate systems
Godot uses a right-handed coordinate system, which means Y+ is up, X+ is
right, and Z- is forward. This means that if you want to move a cube away
from you, you would decrease it's z-position. OpenGL applications generally use
this coordinate system for historical reasons.

Left-handed coordinate systems use Z+ as forward. I personally think of 3D
space in this way and it feels intuitive for me that increasing z-position
moves something away from me.

Nonetheless, compilers don't care about how we use the numbers as long as we
follow a few simple rules: write syntactically valid code. In fact, that's
the only rule (unless you're writing Rust)! One of these rules is that 0 is
the first index of an array, and 1 is the next index, and so on. You will see
some negations in the code when indexing `world`. The reason is because the
playable area in the game starts at (0, 0, 0) and extends forwards (z-) and
to the right (x+), so a tile might be placed at world position (2.0, 0,
-5.0), but -5 is an invalid array index, so it is negated to 5.

### Some clarification on negative indices

When I say that a negative index is "invalid" that is not completely true. It
is, in fact, syntactically valid in some languages, like GDScript and Python.
But usually - at least in my experience - a negative array index just means
that you're starting from the back of the array instead of the front. So -1
will be the last index, -2 the second last, and so on. In reality a negative
array index can be expressed as `array[array.length - index]`.

## Pick
[Simple picking has been discussed in the previous post](../tilePicking)

The remaining offset of `Vector3(0.5, 0, 0.5)` is due to the fact that each
tile stretches from -0.5 to 0.5 in the x and z direction. So a tile might
have the following corners:  
```
(2.5, 0, 0.5)  +----------+  (3.5, 0, 0.5)
               |          |
               |          |
               |          |
(2.5, 0, -0.5) +----------+  (3.5, 0, -0.5)
```
Offsetting each corner gives us:
```
(3.0, 0, 1.0)  +----------+  (4.0, 0, 1.0)
               |          |
               |          |
               |          |
(3.0, 0, 0.0)  +----------+  (4.0, 0, 0.0)
```
And we can simply grab the bottom-left corner to get the tile's position.

## Actual code

```gdscript
const Conveyor = preload("res://conveyor.tscn")

var world = []

var world_size_x = 0
var world_size_z = 0

enum ConveyorType {
    None = 0,
    X_Pos = 1, X_Pos_Z_Pos, X_Pos_Z_Neg,
    X_Neg = 2, X_Neg_Z_Pos, X_Neg_Z_Neg,
    Z_Pos = 3, Z_Pos_X_Pos, Z_Pos_X_Neg,
    Z_Neg = 4, Z_Neg_X_Pos, Z_Neg_X_Neg,
    Cross,
}

func _init(world_size_x: int, world_size_z: int):
    self.world_size_x = world_size_x
    self.world_size_z = world_size_z
    
    world.resize(world_size_z)
    for z in range(world_size_z):
        var row = []
        row.resize(world_size_x)
        for x in range(world_size_x):
            row[x] = ConveyorType.None
        world[z] = row
   
func pick(ray_origin: Vector3, ray_direction: Vector3): # highlight-line link{../tile-picking} text{This is explained in detail here}
    var dist = -ray_origin.y / ray_direction.y
    var hit = (ray_origin + ray_direction * dist) + Vector3(0.5, 0, 0.5)
    return [floor(hit.x), -floor(hit.z)]

func add_conveyor(parent_x: int, parent_z: int, position_x: int, position_z: int):
    var conveyor: Node = Conveyor.instance()
    conveyor.transform.origin = Vector3(position_x, 0, -position_z)
    
    if !is_valid(parent_x, parent_z) || !is_valid(position_x, position_z):
        return
    if parent_x == position_x && parent_z == position_z:
        return
    if parent_x != position_x && parent_z != position_z:
        return
    
    var parent = get_at(parent_x, parent_z)
    var new_parent = parent
    var new_tile

    if position_x > parent_x: # highlight-start text{This code will be improved later to handle corners and crosses}
        new_tile = ConveyorType.X_Pos
        new_parent = parent if parent == ConveyorType.X_Pos else ConveyorType.Cross
    elif position_x < parent_x:
        new_tile = ConveyorType.X_Neg
        new_parent = parent if parent == ConveyorType.X_Neg else ConveyorType.Cross
    elif position_z > parent_z:
        new_tile = ConveyorType.Z_Neg
        new_parent = parent if parent == ConveyorType.Z_Neg else ConveyorType.Cross
    elif position_z < parent_z:
        new_tile = ConveyorType.Z_Pos
        new_parent = parent if parent == ConveyorType.Z_Pos else ConveyorType.Cross # highlight-end
        
    set_at(parent_x, parent_z, new_parent)
    set_at(position_x, position_z, new_tile)

func is_valid(x: int, z: int):
    return x >= 0 && x < world_size_x && z >= 0 && z < world_size_z

func set_at(x: int, z: int, type):
    world[z][x] = type

func get_at(x: int, z: int): # highlight-line text{This function can be used if you know x and z is valid}
    return world[z][x]

func try_get_at(x: int, z: int): #highlight-line text{This function can be used if you don't know if x and z are valid}
    return world[z][x] if is_valid(x, z) else null
```

## And finally, we:
- add a .gitignore file
- add all of our project files
- optionally (but recommended!) set our remote location to our repo on gitlab/github/bitbucket
- push our code so that it ends up online, giving us a warm feeling inside,
  knowing that the code is backed up somewhere off our computers

```
git add .gitignore
git add conveyor.tscn conveyorCross.tscn icon.png project.godot scripts test.tscn
git commit -v
git remote add origin git@gitlab.com:DavidLyhedDanielsson/godot.git
git push
```
